<a href="create.php">Add Favorite Mobile Model</a>
<?php
include_once("../../../vendor/autoload.php");
use MobileApp\Seip50\Mobile\Mobile;

$ob = new Mobile();
$data = $ob->index();

//print_r($data);
?>
<?php
if (isset($_SESSION['Message']) && !empty($_SESSION['Message'])) {
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}

?>
<table border="1">
    <tr>

        <th>Data</th>
        <th colspan="3">Action</th>
    </tr>
    <?php
    
    if(isset($data) && !empty($data)){

    foreach ($data as $item) {

        ?>
        <tr>
            <td><?php echo $item['title'] ?></td>
            <td><a href="show.php?id=<?php echo $item['id']?>">View</a></td>
            <td><a href="edit.php?id=<?php echo $item['id']?>">Edit</td>
            <td><a href="delete.php?id=<?php echo $item['id']?>">Delete</td>
        </tr>
    <?php }} ?>
</table>
